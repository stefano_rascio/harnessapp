#pragma once

#include "ofMain.h"
#include "ofxOsc.h"


#include <cstdint>


class ofApp : public ofBaseApp
{
public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

private:
	ofTrueTypeFont font_;

	ofxOscSender sender_;
	ofxOscReceiver receiver_;

	std::string host_		= "10.40.20.132";
	uint32_t sendingPort_   = 9001;
	uint32_t receivingPort_ = 9003;
	bool loopToggle_		= false;
};
