#include "ofApp.h"


#include <type_traits>


namespace
{
void sendOscMessage (ofxOscSender& sender, const std::string& content)
{	
		ofxOscMessage m;
		m.setAddress (content);

		sender.sendMessage (m, false);
}

void sendOscInt32Message (ofxOscSender& sender, const std::string& address, int32_t argument)
{
	ofxOscMessage m;
	m.setAddress  (address);
	m.addInt32Arg (argument);

	sender.sendMessage (m, false);
}


const std::string playMessage	 ("/mov/play");
const std::string pauseMessage	 ("/mov/pause");
const std::string restartMessage ("/mov/restart"); 
const std::string stopMessage	 ("/mov/stop");
const std::string prevMessage	 ("/mov/prev");
const std::string nextMessage	 ("/mov/next");
const std::string setLoopMessage ("/mov/setLoop");
}


//--------------------------------------------------------------
void ofApp::setup()
{
	ofBackground (40, 100, 40);

	// open an outgoing connection to HOST:PORT
	sender_.setup   (host_, sendingPort_);
	receiver_.setup (receivingPort_);
}

//--------------------------------------------------------------
void ofApp::update()
{
	// check for waiting messages
	while (receiver_.hasWaitingMessages() )
	{
		// get the next message
		ofxOscMessage m;
		receiver_.getNextMessage(m);

		if		(m.getAddress() == playMessage)    { keyPressed (']'); }
		else if (m.getAddress() == pauseMessage)   { keyPressed ('['); }
		else if (m.getAddress() == restartMessage) { keyPressed ('s'); }
		else if (m.getAddress() == prevMessage)    { keyPressed ('p'); }
		else if (m.getAddress() == nextMessage)    { keyPressed ('n'); }
		else if (m.getAddress() == setLoopMessage) { keyPressed ('l'); }
		else                                       { std::cout << "unknown message: " << m.getAddress() << std::endl; }
	}
}

//--------------------------------------------------------------
void ofApp::draw()
{
	// display instructions
	ofDrawBitmapString ("sending osc messages to: " + host_ + ":" + std::to_string (sendingPort_),		 10, 20);
	ofDrawBitmapString ("press ] to send osc message [" + playMessage + "]",							 10, 50);
	ofDrawBitmapString ("press [ to send osc message [" + pauseMessage + "]",							 10, 65);
	ofDrawBitmapString ("press s to send osc message [" + restartMessage + "and [" + stopMessage + "]",	 10, 80);
	ofDrawBitmapString ("press p to send osc message [" + prevMessage + "]",							 10, 95);
	ofDrawBitmapString ("press n to send osc message [" + nextMessage + "]",							 10, 110);
	ofDrawBitmapString ("press l to send osc message [" + setLoopMessage + "]",							 10, 125);

	ofDrawBitmapString ("receiving osc messages on: " + std::to_string (receivingPort_),				 10, 155);
}

//--------------------------------------------------------------
void ofApp::keyPressed (int key)
{
	switch (key)
	{
	    case ']': { sendOscMessage	    (sender_, playMessage);																			   break; }
	    case '[': { sendOscMessage	    (sender_, pauseMessage);																		   break; }
	    case 's': { sendOscMessage	    (sender_, restartMessage);    sendOscMessage (sender_, stopMessage);							   break; }
	    case 'p': { sendOscMessage	    (sender_, prevMessage);																			   break; }
	    case 'n': { sendOscMessage	    (sender_, nextMessage);																			   break; }
	    case 'l': { sendOscInt32Message (sender_, setLoopMessage, static_cast <int32_t> (!loopToggle_) );   loopToggle_ = !loopToggle_;    break; }
	    default:																														   break;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
